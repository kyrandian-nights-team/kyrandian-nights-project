# c_province37

# County Title
title = c_province37

# Settlements
max_settlements = 5

b_b1p37 = castle
b_b2p37 = city
b_b3p37 = temple
#b_b4p37 = castle
#b_b5p37 = city

# Misc
culture = hungarian
religion = norse_pagan
terrain = plains
#History
