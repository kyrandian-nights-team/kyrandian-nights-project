# c_province21

# County Title
title = c_province21

# Settlements
max_settlements = 5

b_b1p21 = castle
b_b2p21 = city
b_b3p21 = temple
#b_b4p21 = castle
#b_b5p21 = city

# Misc
culture = hungarian
religion = norse_pagan
terrain = plains
#History
