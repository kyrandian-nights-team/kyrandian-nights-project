# c_province41

# County Title
title = c_province41

# Settlements
max_settlements = 5

b_b1p41 = castle
b_b2p41 = city
b_b3p41 = temple
#b_b4p41 = castle
#b_b5p41 = city

# Misc
culture = hungarian
religion = norse_pagan
terrain = plains
#History
