# c_province32

# County Title
title = c_province32

# Settlements
max_settlements = 5

b_b1p32 = castle
b_b2p32 = city
b_b3p32 = temple
#b_b4p32 = castle
#b_b5p32 = city

# Misc
culture = hungarian
religion = norse_pagan
terrain = plains
#History
