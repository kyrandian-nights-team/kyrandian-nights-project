# c_province22

# County Title
title = c_province22

# Settlements
max_settlements = 5

b_b1p22 = castle
b_b2p22 = city
b_b3p22 = temple
#b_b4p22 = castle
#b_b5p22 = city

# Misc
culture = hungarian
religion = norse_pagan
terrain = plains
#History
